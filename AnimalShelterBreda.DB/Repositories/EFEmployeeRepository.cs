﻿using AnimalShelterBreda.Core.DomainModel;
using AnimalShelterBreda.Core.DomainServices;
using AnimalShelterBreda.DB.Data;
using System.Linq;

namespace AnimalShelterBreda.DB.Repositories
{
    public class EFEmployeeRepository : IEmployeeRepository
    {
        private readonly AppDbContext context;

        public EFEmployeeRepository(AppDbContext ctx)
        {
            context = ctx;
        }

        public IQueryable<Employee> Employees => context.Employees;

        public void DeleteEmployee(int EmployeeID)
        {
            throw new System.NotImplementedException();
        }

        public void SaveEmployee(Employee employee)
        {
            // Geen ID gevonden -> Voeg nieuwe werknemer toe
            if(employee.Id == 0)
            {
                context.Employees.Add(employee);
            }
            else
            {
                // Bestaat de werknemer -> Update eigenschappen
                Employee dbEntry = context.Employees.FirstOrDefault(e => e.Id == employee.Id);
                
                if(dbEntry != null)
                {
                    dbEntry.FirstName = employee.FirstName;
                    dbEntry.MiddleName = employee.MiddleName;
                    dbEntry.SirName = employee.SirName;
                    dbEntry.Email = employee.Email;
                    dbEntry.PhoneNumber = employee.PhoneNumber;
                    dbEntry.DateOfBirth = employee.DateOfBirth;
                    dbEntry.Password = employee.Password;
                    dbEntry.IsVolunteer = employee.IsVolunteer;
                    dbEntry.Notes = employee.Notes;
                }
            }
            context.SaveChanges();
        }

    }
}