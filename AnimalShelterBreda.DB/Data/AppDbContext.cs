﻿using AnimalShelterBreda.Core.DomainModel;
using Microsoft.EntityFrameworkCore;


namespace AnimalShelterBreda.DB.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        public DbSet<Animal> Animals { get; set; }
        public DbSet<AnimalForAdoption> AnimalsForAdoption { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Note> Notes { get; set; }
        public DbSet<Shelter> Shelters { get; set; }
        public DbSet<Treatment> Treatments { get; set; }
    }
}
