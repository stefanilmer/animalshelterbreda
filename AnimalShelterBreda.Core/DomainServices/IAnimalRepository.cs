﻿using AnimalShelterBreda.Core.DomainModel;
using System.Linq;

namespace AnimalShelterBreda.Core.DomainServices
{
    public interface IAnimalRepository
    {
        // Lijst met alle dieren
        IQueryable<Animal> Animals { get; }

        // Animal CRUD
        void SaveAnimal(Animal animal);
        void UpdateAnimal(Animal animal);
        void DeleteAnimal(int AnimalID);
    }
}
