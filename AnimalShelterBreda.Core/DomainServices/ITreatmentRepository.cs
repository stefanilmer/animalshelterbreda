﻿using AnimalShelterBreda.Core.DomainModel;
using System.Linq;

namespace AnimalShelterBreda.Core.DomainServices
{
    public interface ITreatmentRepository
    {
        IQueryable<Treatment> Treatments { get; }

        void SaveTreatment(Treatment treatment);
        void UpdateTreatment(Treatment treatment);
        void DeleteTreatment(int treatmentId);
    }
}
