﻿using AnimalShelterBreda.Core.DomainModel;
using System.Linq;

namespace AnimalShelterBreda.Core.DomainServices
{
    public interface INoteRepository
    {
        IQueryable<Note> Notes { get; }

        void SaveNote(Note note);
        void UpdateNote(Note note);
        void DeleteNote(int noteId);
    }
}
