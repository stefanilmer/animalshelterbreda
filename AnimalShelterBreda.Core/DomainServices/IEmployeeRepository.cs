﻿using AnimalShelterBreda.Core.DomainModel;
using System.Linq;

namespace AnimalShelterBreda.Core.DomainServices
{
    public interface IEmployeeRepository
    {
        // Lijst met alle Medewerkers
        IQueryable<Employee> Employees { get; }

        void SaveEmployee(Employee employee);
        void DeleteEmployee(int EmployeeID);
    }
}
