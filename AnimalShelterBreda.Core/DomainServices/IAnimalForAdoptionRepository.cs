﻿using AnimalShelterBreda.Core.DomainModel;
using System.Linq;

namespace AnimalShelterBreda.Core.DomainServices
{
    public interface IAnimalForAdoptionRepository
    {
        IQueryable<AnimalForAdoption> AnimalsForAdoption { get; }

        void SaveAnimalForAdoption(AnimalForAdoption animalForAdoption);
        void UpdateAnimalForAdoption(AnimalForAdoption animalForAdoption);
        void DeleteAnimalForAdoption(int animalForAdoptionID);
    }
}
