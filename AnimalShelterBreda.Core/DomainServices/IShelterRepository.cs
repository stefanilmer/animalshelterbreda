﻿using AnimalShelterBreda.Core.DomainModel;
using System.Linq;

namespace AnimalShelterBreda.Core.DomainServices
{
    public interface IShelterRepository
    {
        IQueryable<Shelter> Shelters { get; }

        void SaveShelter(Shelter shelter);
        void UpdateShelter(Shelter shelter);
        void DeleteShelter(int shelterID);
    }
}
