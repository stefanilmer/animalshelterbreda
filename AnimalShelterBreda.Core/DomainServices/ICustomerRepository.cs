﻿using AnimalShelterBreda.Core.DomainModel;
using System.Linq;

namespace AnimalShelterBreda.Core.DomainServices
{
    public interface ICustomerRepository
    {
        IQueryable<Customer> Customers { get; }

        void SaveCustomer(Customer customer);
        void UpdateCustomer(Customer customer);
        void DeleteCustomer(int customer);
    }
}
