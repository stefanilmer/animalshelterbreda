﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnimalShelterBreda.Core.DomainModel
{
    public enum Treatments
    {
        Sterilisatie,
        Castratie,
        Inenting,
        Operatie,
        Chippen,
        Euthanasie
    }

    public class Treatment : EF
    {
        [Required(ErrorMessage = "Een behandelings type is verplicht.")]
        public Treatment TreatmentType { get; set; }
        public string Description { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Costs { get; set; }

        // Int die aangeeft dat een operatie pas 
        // mag worden uitgevoerd nadat het dier X maanden oud is.
        public int AllowedAfterMonths { get; set; }

        // Naam van de persoon die de behandeling heeft uitgevoerd.
        [Required(ErrorMessage = "Vul een naam in van wie de behandeling heeft uitgevoerd.")]
        public string PerformedBy { get; set; }

        // Datum en tijd wanneer de behandeling is uitgevoerd.
        [Required(ErrorMessage = "Vul een datum/tijd in waneer de behandeling is uitgevoerd.")]
        public DateTime PerformedWhen { get; set; }

        // Relations
        // Animal that was performed on
        public Animal AnimalForTreatment { get; set; }

        public void SetTreatmentType(Treatment treatmenType)
        {
            TreatmentType = treatmenType;
        }
        
        public Treatment GetTreatmentType()
        {
            return TreatmentType;
        }
    }
}

// TODO: Waarbij verplicht een Opmerking gemaakt moet worden, Krijgt de gebruiker een 
// popup voor het invullen van een opmerking

// TODO: Get en set van enum implementeren in interface met checks
// In de interface set and get maken met parameters
//
// Inenting(Bij het geven van de inenting is het verplicht om een omschrijving in te vullen)
// Operatie(Het type operatie moet hierbij verplicht in worden gevuld bij de opmerkingen)
// Chippen(de chipcode(een GUID) moet hierbij verplicht in worden gevuld bij de opmerkingen)
// Euthanasie(de reden van euthanasie moet hierbij verplicht in worden gevuld bij de opmerkingen)
