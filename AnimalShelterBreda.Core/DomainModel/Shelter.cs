﻿using System.Collections.Generic;

namespace AnimalShelterBreda.Core.DomainModel
{
    public enum ShelterTypes
    {
        Group,
        Individual
    }

    public class Shelter : EF
    {
        public ShelterTypes ShelterType { get; set; }
        public string Species { get; set; }
        public bool IsCastrated { get; set; }

        // Nullable omdat null = mix gender, 0 = vrouwen, 1 = mannen
        public bool? Gender { get; set; }

        public int MaximumCapacity { get; set; }
        public int CurrentCapacity { get; set; }

        // Relations
        public List<Animal> AnimalsInShelter { get; set; }


        // Methods
        public virtual void SetShelterType(ShelterTypes type)
        {
            ShelterType = type;
        }

        public virtual ShelterTypes GetShelterType()
        {
            return ShelterType;
        }
    }


}
