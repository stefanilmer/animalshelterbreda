﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AnimalShelterBreda.Core.DomainModel
{
    public class Customer : EF
    {
        [Required(ErrorMessage = "Vul een voornaam in.")]
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        [Required(ErrorMessage = "Vul een achternaam in.")]
        public string SirName { get; set; }

        [Required(ErrorMessage = "Vul een emailadres in.")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "Vul een telefoonnummer in.")]
        [Phone]
        public int PhoneNumber { get; set; }

        [Required(ErrorMessage = "Vul een geboortedatum in.")]
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }

        [Required(ErrorMessage = "Vul een straatnaam in.")]
        public string StreetName { get; set; }

        [Required(ErrorMessage = "Vul een huisnummer in.")]
        public int HouseNumber { get; set; }

        public string HouseNumberAddition { get; set; }

        [Required(ErrorMessage = "Vul een postcode in.")]
        [DataType(DataType.PostalCode)]
        public string PostalCode { get; set; }

        [Required(ErrorMessage = "Vul een stad in.")]
        public string City { get; set; }

        [Required(ErrorMessage = "vul een wachtwoord in")]
        [PasswordPropertyText]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        // Relations

        public List<Animal> AdoptedAnimals { get; set; }
        public List<AnimalForAdoption> AnimalsForAdoption { get; set; }

    }
}
