﻿using System.ComponentModel.DataAnnotations;

namespace AnimalShelterBreda.Core.DomainModel
{
    public class AnimalForAdoption : EF
    {
        [Required(ErrorMessage = "Vul een naam in.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Vul een dierensoort in.")]
        public string Species { get; set; }

        [Required(ErrorMessage = "Vul een geslacht in.")]
        public bool Gender { get; set; }

        // Of een dier gecastreerd is
        [Required(ErrorMessage = "Vul in of het dier is gecastreerd.")]
        public bool IsCastrated { get; set; }

        [Required(ErrorMessage = "Geef een reden waarom het dier ter adoptie wordt gegeven.")]
        public string ReasonAdoption { get; set; }
    }
}
