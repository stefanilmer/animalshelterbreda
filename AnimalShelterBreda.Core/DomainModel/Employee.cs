﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AnimalShelterBreda.Core.DomainModel
{
    public class Employee : EF
    {
        [Required(ErrorMessage = "Vul een voornaam in.")]
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        [Required(ErrorMessage = "Vul een achternaam in.")]
        public string SirName { get; set; }

        [Required(ErrorMessage = "Vul een emailadres in.")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "Vul een telefoonnummer in.")]
        [Phone]
        public int PhoneNumber { get; set; }

        [Required(ErrorMessage = "Vul een geboortedatum in.")]
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }

        [Required(ErrorMessage = "vul een wachtwoord in")]
        [PasswordPropertyText]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Vul in of de persoon een vrijwilliger is")]
        public bool IsVolunteer { get; set; }

        // Relations
        public List<Note> Notes { get; set; }
    }
}
