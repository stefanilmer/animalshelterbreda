﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AnimalShelterBreda.Core.DomainModel
{
    public class Note : EF
    {
        [Required(ErrorMessage = "Vul een opmerking in.")]
        public string Content { get; set; }

        [Required(ErrorMessage = "Vul de datum van de opmerking in.")]
        [DataType(DataType.Date)]
        public DateTime DateNoteWritten { get; set; }

        // relations
        public Employee Author { get; set; }
        public Animal AboutAnimal { get; set; }
    }
}
