﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AnimalShelterBreda.Core.DomainModel
{
    public class Animal : EF
    {
        [Required(ErrorMessage = "Vul een naam van het dier in.")]
        public string Name { get; set; }

        public DateTime DateOfBirth { get; set; }

        // Age gets calculated using DateOfBirth if availible
        public int Age { get; set; }

        // Esitamted Age incase no DateOfBirth is known. Needs to be validated
        public int EsimatedAge { get; set; }

        [Required(ErrorMessage = "Vul een beschrijving in van het dier.")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Vul een diersoort in.")]
        public string Species { get; set; }

        public string Race { get; set; }

        [Required(ErrorMessage = "Vul een geslacht in.")]
        public bool Gender { get; set; }

        public string FotoUrl { get; set; }

        [Required(ErrorMessage = "Vul de datum van binnenkomst in.")]
        public DateTime ArivalDate { get; set; }

        // Datum wanneer een dier is geadopteerd
        public DateTime AdoptionDate { get; set; }

        // Datum wanneer is dier is overleden
        public DateTime DeceasedDate { get; set; }

        // Of een dier gecastreerd is
        [Required(ErrorMessage = "Vul in of het dier is gecastreerd.")]
        public bool IsCastrated { get; set; }

        // Of een dier met kinderen om kan gaan
        [Required(ErrorMessage = "Vul in of het dier om kan gaan met kinderen.")]
        public bool AllowedWithChilderen { get; set; }

        // Opmerkingen worden gebruikt voor Medewerkers/Vrijwilligers
        public string Note { get; set; }

        // TODO: Kan alleen gezien worden door de Medewerkers en Vrijwilligers?
        [Required(ErrorMessage = "Geef een reden waarom het dier ter adoptie wordt gegeven.")]
        public string ReasonAdoption { get; set; }

        // Is het dier beschikbaar om geadopteerd te worden
        public bool CanAdopt { get; set; }


        // Relations
        // Lijst met opmerkingen over het dier
        public List<Note> Notes { get; set; }

        // Lijst met behandelingen van het dier
        public List<Treatment> Treatments { get; set; }

        // CustomerID van de nieuwe eigenaar
        public Customer AdoptedBy { get; set; }

        // VerblijfID van het huidige verblijf
        //public int ShelterId { get; set; }

        // Het verblijf waar het dier in zit
        public Shelter CurrentShelter { get; set; }
    }

}
